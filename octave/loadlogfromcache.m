function L=loadlogfromcache(fname)
% LOADLOGFROMCACHE  Load heater log file via cache

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

persistent logcache

if isempty(logcache)
  logcache.fname={};
  logcache.fmodtime=[];
  logcache.data={};
end

if all(strcmp(fname,logcache.fname) == 0)
  fprintf('Log file %s is not in cache. Loading it.\n', fname);
  fflush(stdout);

  L=loadheaterlog(fname);

  % Insert into cache
  logcache.fname{end+1}=fname;
  d=dir(fname);
  logcache.fmodtime(end+1)=d.datenum;
  logcache.data{end+1}=L;
else
  % Found in cache
  idx=find(strcmp(fname,logcache.fname));
  d=dir(fname);
  if d.datenum > logcache.fmodtime(idx)
    fprintf('Log file %s is newer than cached version. Loading it.\n', fname);
    fflush(stdout);
    L=loadheaterlog(fname);

    % Update cache
    logcache.fmodtime(idx)=d.datenum;
    logcache.data{idx}=L;
  else
    fprintf('Log file %s is in cache!\n', fname);
    fflush(stdout);
    L=logcache.data{idx};
 end
end


