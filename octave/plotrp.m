function plotrp(T,P,GL,GR,txg,avg_gain,plot_title)
% PLOTRP  Annotated LHCP and RHCP radiation pattern plot
%
% plotrp(T, P, GL, GR, txg, avg_gain, plot_title)
%
% T, P are the matrices of zenith and azimuth angles for the radiation
% patterns. GL and GR are the corresponding matrices of gains (in dB) for
% LHCP and RHCP respectively.
%
% txg is a Tx group structure (see TXGROUPS).
%
% avg_gain is the average total gain in dB, which is shown on the plot but
% does not affect any gain calculations. (It is assumed that the average
% gain correction has already been applied to GL and GR).
%
% plot_title is a string used to title the plot. If empty or missing,
% a title based on the date, time, array and frequency is used.

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

if nargin < 7
  plot_title='';
end

pwr_tot=1e3*sum(txg.pwr);

psize=[11.69 8.27]; % A4, in inches (Octave 3.2.4 doesn't like cm)
ppos=[0 0 psize];

v=ver('Octave');
[vnums,vc,vemsg]=sscanf(v.Version, '%d.%d');
if vc ~= 2
  error('Unable to parse Octave version number');
end
if all(vnums==[3; 8]) || all(vnums==[4; 0])
  warning('Doing workaround for landscape printing bug')
  ppos([3 4])=ppos([4 3]);
end

fh=figure;
set(fh,'paperunits','inches','papersize',psize, ...
    'paperorientation','landscape','paperposition',ppos);
colormap(jet(10));

s=psize(1)/psize(2);

%---- Create all the axes ----

% Header and footer
ah_head=axes('units','normalized','position',[0 0.95 1 0.05]);
ah_foot=axes('units','normalized','position',[0 0 1 0.05]);

% Full radiation pattern plots
wi=0.35;
ah1=axes('units','normalized','position',[0.25-0.5*wi 0.95-wi*s wi wi*s]);
ah2=axes('units','normalized','position',[0.75-0.5*wi 0.95-wi*s wi wi*s]);

% Colorbar
cbwi=0.05;
ah_cb=axes('units','normalized','position',[0.5-0.5*cbwi 0.95-wi*s cbwi wi*s]);

% North-south slices of radiation patterns
ah_lp=axes('units','normalized', ...
           'position',[0.25-0.5*wi 0.95-wi*s-0.02-0.5*wi*s wi 0.5*wi*s]);
ah_rp=axes('units','normalized', ...
           'position',[0.75-0.5*wi 0.95-wi*s-0.02-0.5*wi*s wi 0.5*wi*s]);


% Information boxes
pos=get(ah_lp,'position');
txthi=pos(2)-0.05-0.02;
ah_ltxt=axes('units','normalized', ...
             'position',[0.25-0.5*wi 0.05 wi txthi]);
ah_rtxt=axes('units','normalized', ...
             'position',[0.75-0.5*wi 0.05 wi txthi]);


%---- Plot full radiation patterns ----

[GR_max,GR_idx]=max(GR(:));
[GL_max,GL_idx]=max(GL(:));
G_max=max([GL_max GR_max]);
X=T.*cosd(P); Y=T.*sind(P);
% Compass azimuth angle
% Increases clockwise from zero at north.
AZ=90-P;
AZ(AZ < 0)=AZ(AZ < 0)+360;

% When calling contourf, the matrices are extended by repeating
% the first column to avoid a gap in the plot between 359 and 0 degrees
% P angle.

axes(ah1)
contourf([X X(:,1)],[Y Y(:,1)],[GL GL(:,1)]-G_max,[-60:6:-6 -3]);
polargrid;
xlim([-90 90]);
ylim([-90 90]);
caxis([-60 0]);
axis(ah1,'off');
text(1,0.5,'E','units','normalized','parent',ah1);
text(0.5,1,'N','units','normalized','parent',ah1,'backgroundcolor',[1 1 1]);
pts2norm=1./(72*wi*s*psize(2)); % factor to convert points to norm. units
fontsz=20;
text(0.01,1-fontsz*pts2norm/2,'LHCP','fontsize',fontsz, ...
     'units','normalized','parent',ah1);
text(0.9,1-fontsz*pts2norm/2,'X','fontsize',20, ...
     'units','normalized','parent',ah1);


axes(ah2)
contourf([X X(:,1)],[Y Y(:,1)],[GR GR(:,1)]-G_max,[-60:6:-6 -3]);
polargrid
xlim([-90 90]);
ylim([-90 90]);
caxis([-60 0]);
axis(ah2,'off')
text(1,0.5,'E','units','normalized','parent',ah2);
text(0.5,1,'N','units','normalized','parent',ah2,'backgroundcolor',[1 1 1]);
text(0.01,1-fontsz*pts2norm/2,'RHCP','fontsize',fontsz, ...
     'units','normalized','parent',ah2);
text(0.9,1-fontsz*pts2norm/2,'O','fontsize',fontsz, ...
     'units','normalized','parent',ah2);

% The colorbar (common to both plots)

axes(ah_cb)
n=size(get(fh,'colormap'),1);
cl=get(ah1,'clim');
y=linspace(cl(1),cl(2),n+1).';
surface(repmat([0 1],n+1,1),[y y],zeros(n+1,2),[y y]);
ylim(cl);
caxis(cl);
set(ah_cb,'xtick',[],'ytick',y);

%---- North-south slices of radiation pattern ----

gain_lim=40; % maximum gain below peak to show on plot

% The azimuth angle P increases anticlockwise from the X-axis
% which points eastwards.

idx1=find(P(1,:)==90);
idx2=find(P(1,:)==270);
% zenith angle will go from -90 to +90 degrees
t=[-flipud(T(2:end,idx2)); T(:,idx1)];

% LHCP
g=[flipud(GL(2:end,idx2)); GL(:,idx1)];
g=g-G_max+gain_lim;
[lt_max,lg_max,lhwl,lhwr]=plotgainslice(ah_lp,t,g,gain_lim);
lg_max=lg_max-gain_lim+G_max;
text(1,0,'N','units','normalized','parent',ah_lp);
text(-0.03,0,'S','units','normalized','parent',ah_lp);


% RHCP
g=[flipud(GR(2:end,idx2)); GR(:,idx1)];
g=g-G_max+gain_lim;
[rt_max,rg_max,rhwl,rhwr]=plotgainslice(ah_rp,t,g,gain_lim);
rg_max=rg_max-gain_lim+G_max;
text(1,0,'N','units','normalized','parent',ah_rp);
text(-0.03,0,'S','units','normalized','parent',ah_rp);

%---- ERP and gain information ---

ERP_L=pwr_tot*10.^(GL_max/10);
ERP_R=pwr_tot*10.^(GR_max/10);

axis(ah_ltxt,'off');
erpfontsz=20;
pts2norm=1./(72*txthi*psize(2)); % factor to convert points to norm. units
ypos=1-erpfontsz*pts2norm/2;
text(0.01,ypos,sprintf('ERP %.1f MW',ERP_L/1e6),'parent',ah_ltxt, ...
     'units','normalized','fontsize',erpfontsz);

% Peak gain
peak_str=sprintf('Peak gain %.1f dBi at Az %.0f Zen %.0f', ...
                 GL_max,AZ(GL_idx),T(GL_idx));
fontsz=12;
ypos=1-(erpfontsz+fontsz/2+2)*pts2norm;
text(0.01,ypos,peak_str,'parent',ah_ltxt, ...
     'fontsize',12,'units','normalized')

% Gain of opposite polarisation
opp_str=sprintf('RHCP gain %.1f dBi',GR(GL_idx));
fontsz=12;
ypos=1-(erpfontsz+1.5*fontsz+2*2)*pts2norm;
text(0.01,ypos,opp_str,'parent',ah_ltxt, ...
     'fontsize',12,'units','normalized')

% North-south gain details
ns_str=sprintf('N-S peak %.1f dBi at Zen %.1f',lg_max,lt_max);
fontsz=12;
ypos=1-(erpfontsz+2.5*fontsz+3*2)*pts2norm;
text(0.01,ypos,ns_str,'parent',ah_ltxt, ...
     'fontsize',12,'units','normalized')

% North-south beamwidth
bw_str=sprintf('N-S -3 dB beamwidth %.1f deg.',lhwl+lhwr);
fontsz=12;
ypos=1-(erpfontsz+3.5*fontsz+4*2)*pts2norm;
text(0.01,ypos,bw_str,'parent',ah_ltxt, ...
     'fontsize',12,'units','normalized')


axis(ah_rtxt,'off');
erpfontsz=20;
pts2norm=1./(72*txthi*psize(2)); % factor to convert points to norm. units
ypos=1-erpfontsz*pts2norm/2;
text(0.01,ypos,sprintf('ERP %.1f MW',ERP_R/1e6),'parent',ah_rtxt, ...
     'units','normalized','fontsize',erpfontsz);

% Peak gain
peak_str=sprintf('Peak gain %.1f dBi at Az %.0f Zen %.0f', ...
                 GR_max,AZ(GR_idx),T(GR_idx));
fontsz=12;
ypos=1-(erpfontsz+fontsz/2+2)*pts2norm;
text(0.01,ypos,peak_str,'parent',ah_rtxt, ...
     'fontsize',12,'units','normalized')

% Gain of opposite polarisation
opp_str=sprintf('LHCP gain %.1f dBi',GL(GR_idx));
fontsz=12;
ypos=1-(erpfontsz+1.5*fontsz+2*2)*pts2norm;
text(0.01,ypos,opp_str,'parent',ah_rtxt, ...
     'fontsize',12,'units','normalized')

% North-south gain details
ns_str=sprintf('N-S peak %.1f dBi at Zen %.1f',rg_max,rt_max);
fontsz=12;
ypos=1-(erpfontsz+2.5*fontsz+3*2)*pts2norm;
text(0.01,ypos,ns_str,'parent',ah_rtxt, ...
     'fontsize',12,'units','normalized')

% North-south beamwidth
bw_str=sprintf('N-S -3 dB beamwidth %.1f deg.',rhwl+rhwr);
fontsz=12;
ypos=1-(erpfontsz+3.5*fontsz+4*2)*pts2norm;
text(0.01,ypos,bw_str,'parent',ah_rtxt, ...
     'fontsize',12,'units','normalized')

%---- Header and footer ----

if isempty(plot_title)
  header_str=datestr(txg.dn,'YYYY-mm-dd HH:MM:SS');
else
  header_str=plot_title;
end
header_str=[header_str sprintf(' A%d %.8f MHz',txg.arr,txg.freq)];
header_str=[header_str sprintf(' %.1f kW',pwr_tot/1e3)];

text(0.5,0.5,header_str,'horizontalalignment','center', ...
     'parent',ah_head,'fontsize',20,'units','normalized');
axis(ah_head,'off');

[status,hname]=system('hostname');
if status~=0
  hname='unknown machine';
end
foot_str=sprintf('Average gain was %.2f dB. ',avg_gain);
foot_str=[foot_str 'Produced at ' datestr(now,'YYYY-mm-dd HH:MM:SS') ...
          ' on ' hname];
text(0.01,0.5,foot_str,'parent',ah_foot,'fontsize',10,'units','normalized');
axis(ah_foot,'off');

function polargrid

% zenith angles

t=linspace(0,2*pi,1000);
x=cos(t); y=sin(t);

for k=[0 6 12 18 90]
  if k~=90
    c='w';
  else
    c='k';
  end
  line(k*x,k*y,'color',c);
end

% Plot polar gain diagram of a slice through the radiation pattern

function [t_max,g_max,hwl,hwr]=plotgainslice(ah,t,g,gain_lim)

% Refine the position and magnitude of the peak
% NB: assumes 1 degree steps

[g_max,idx]=max(g);
if (idx~=1) && (idx ~= length(g))
  % use quadratic interpolation
  a=(g(idx-1)+g(idx+1)-2*g(idx))/2;
  b=(g(idx+1)-g(idx-1))/2;
  t_max=t(idx)-b/(2*a);
  g_max=g(idx)-b^2/(4*a);
else
  t_max=t(idx);
end

% Estimate -3 dB beamwidth
% (Does not assume a step size)

hwr=nan;
hwl=nan;
q=g-g_max+3;

% right half-width
k=find(q(idx:end) <= 0);
if ~isempty(k)
  k=k(1);
  if q(idx+k-1)==0
    hwr=t_max-t(idx+k-1);
  else
    t_int=t(idx+k-2)-q(idx+k-2)*(t(idx+k-1)-t(idx+k-2))/(q(idx+k-1)-q(idx+k-2));
    hwr=t_int-t_max;
  end
end

% left half-width
k=find(q(1:idx) <= 0);
if ~isempty(k)
  k=k(end);
  if q(k)==0
    hwr=t(k)-t_max;
  else
    t_int=t(k)-q(k)*(t(k+1)-t(k))/(q(k+1)-q(k));
    hwl=t_max-t_int;
  end
end

g(g < 0)=0; % suppress negative values for plotting

axes(ah);
% gain rings
t0=linspace(0,pi,1000);
for g0=10:10:gain_lim
  if g0==gain_lim
    col=[0 0 0];
  else
    col=[0.5 0.5 0.5];
  end
  line(g0*cos(t0),g0*sin(t0),'color',col);
  text(g0*cosd(20),g0*sind(20),sprintf('%d',g0-gain_lim));
end
% ground
line(gain_lim*[-1 1],[0 0]);
% significant zenith angles
for za=-12:6:12
  line([0 gain_lim*sind(za)],[0 gain_lim*cosd(za)],'color',0.5*[1 1 1]);
end
% direction of peak and half widths
line([0 gain_lim*sind(t_max)],[0 gain_lim*cosd(t_max)],'color','r');
line([0 gain_lim*sind(t_max-hwr)],[0 gain_lim*cosd(t_max-hwr)],'color','b');
line([0 gain_lim*sind(t_max+hwl)],[0 gain_lim*cosd(t_max+hwl)],'color','b');
% finally, the polar diagram itself!
line(g.*sind(t),g.*cosd(t));
xlim(gain_lim*[-1 1]);
ylim([0 gain_lim]);
axis(ah,'off');

