function [T,P,G,Et,Ep,R]=readnecrp(fname)
% READNECRP  Read radiation pattern from NEC output file
%
% [T, P, G, Et, Ep, R] = readnecrp(fname)
%
% Read radiation pattern from file fname. The values of the co-ordinates
% theta, phi and the total gain (dBi) are returned in T, P and G. The
% theta and phi components of the electric field are returned in Et and Ep.
% The axial ratio of the polarisation ellipse is returned in R.

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

fid=fopen(fname);
if fid==-1
  error('cannot open input file');
end

% scan for start of radiation pattern

rp_found=0;

while 1
  l=fgetl(fid);
  if l==-1
    break;
  end

  if ~isempty(strfind(l,'- RADIATION PATTERNS -'))
    fprintf('found radiation pattern\n');
    rp_found=1;
    break;
  end
end

if rp_found==0
  fclose(fid);
  error('radiation pattern not found');
end

% skip 4 lines

for k=1:4
  l=fgetl(fid);
end

c=1;
RP=[];

while 1
  l=fgetl(fid);
  if (l==-1) || isempty(l)
    break;
  end
  [x,count]=sscanf(l,'%f %f %f %f %f %f %f %*s %f %f %f %f');
  if count ~= 11
    % Apparently the polarisation string sometimes fails to appear,
    % so try again.
    [x,count]=sscanf(l,'%f %f %f %f %f %f %f %f %f %f %f');
    if count ~= 11  
      error('Unrecognised format in radiation pattern');
    end
  end

  RP(c,1:11)=x;
  c=c+1;
end

if ~isempty(RP)
  T=RP(:,1);
  P=RP(:,2);
  G=RP(:,5);
  R=RP(:,6);
  Et=RP(:,8).*exp(i*RP(:,9)*pi/180);
  Ep=RP(:,10).*exp(i*RP(:,11)*pi/180);
  Tmax=max(T);
  Tmin=min(T);
  if Tmax==Tmin
    nrows=1;
  else
    nrows=(max(T)-min(T))/(T(2)-T(1)) + 1;
  end
  T=reshape(T,nrows,[]);
  P=reshape(P,nrows,[]);
  G=reshape(G,nrows,[]);
  R=reshape(R,nrows,[]);
  Et=reshape(Et,nrows,[]);
  Ep=reshape(Ep,nrows,[]);
else
  T=[];
  P=[];
  G=[];
  R=[];
  Et=[];
  Ep=[];
end

fclose(fid);

