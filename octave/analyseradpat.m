function [GL,GR,G_avg,A]=analyseradpat(T,P,G,Et,Ep)
% ANALYSERADPAT  Analyse radiation pattern
%
% [GL, GR, G_avg, A]=analyseradpat(T, P, G, Et, Ep)
%
% T, P, G, Et, Ep are matrices of the same size giving the zenith and
% azimuth angles, the total gain and the zenithal and azimuthal components
% of the electric field respectively for each point in the radiation pattern.
%
% GL and GR are matrices of the same size as the inputs giving the gains for
% LHCP and RHCP respectively.
%
% G_avg is the average total power gain (linear, not dB) and A is the
% averaging area in steradians. Ideally, for a model with perfect ground,
% G_avg=2. Deviation of G_avg from the ideal can be used to correct the gains.

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% Find magnitudes of the left- and right-hand circular components
% of the electric field

ER_mag=0.5*sqrt((real(Et)-imag(Ep)).^2 + (imag(Et)+real(Ep)).^2);
EL_mag=0.5*sqrt((real(Et)+imag(Ep)).^2 + (-imag(Et)+real(Ep)).^2);

% Proportion of total power in LHCP and RHCP

Ptot=ER_mag.^2+EL_mag.^2;
PR=ER_mag.^2./Ptot;
PL=EL_mag.^2./Ptot;

% Gain for LHCP and RHCP

GR=G+10*log10(PR);
GL=G+10*log10(PL);

% If electric fields were zero, force gain to zero too.
k=find(abs(Et==0) & (Ep==0));
GR(k)=-inf;
GL(k)=-inf;

% Average total power gain
% NB: this is not necessarily exactly how NEC2 does it!

dT=(T(2,1)-T(1,1))*pi/180;
dP=(P(1,2)-P(1,1))*pi/180;

% all points except zenith
dA=sind(T)*dT*dP;
dA(T==0)=0;
dA(T==90)=0.5*dA(T==90);
A=sum(dA(:));
G_avg=sum(dA(:).*10.^(G(:)/10));
% zenith point
dA=dT^2*pi/4;
A=A+dA;
G_avg=(G_avg+dA*10.^(G(1,1)/10))/A;




