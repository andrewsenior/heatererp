function recs=loadheaterlog(fname)
% LOADHEATERLOG  Load all heater log entries
%
% L = loadheaterlog(fname)
%
% Load all entries from heater transmission log 'fname', returning
% the data in a structure L with the following fields:
%
% type     n-element cell array of record types. Possible values:
%          'expsetup' - PET 'Experiment setup' table
%          'tunedvals' - PET 'Tuned values' table
%          'optable' - PET 'Operating Table'
%          'status' - PET/early EROS 'Status at' table
%          'timestamp' - EROS transmitter status table
% dn       n-element vector of datenums of each entry
% freq     12 x n array of transmitter frequencies
% arr      12 x n array of array numbers
% pha      12 x n array of phases
% swr      12 x n array of SWRs
% pwr      12 x n array of powers
% ref_req  2 x n array of reference frequencies
% cmnt     n-element cell array of comment strings
% pwr_a    12 x n array of "analogue" powers
% cap1     12 x n array of tuning capacitor C1 values
% cap2     12 x n array of tuning capactior C2 values
% f_pha    12 x n array of measured forward phases
% r_pha    12 x n array of measured reverse phases
% dds_amp  12 x n array of DDS amplitudes
% dds_pha  12 x n array of DDS phases
% band     12 x n array of band numbers
% mode     12 x n array of mode (polarisation) numbers
% beamdir  12 x n array of beam directions
%
% For PET log files, the array numbers from each 'Experiment setup' table
% are used to fill in this information in subsequent 'Tuned values',
% 'Status at' and 'Operating Table' records which lack this information.
%
% Note that 'Experiment setup' and 'Tuned values' records do not have
% timestamps, so their dn values are NaN.
%

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.


fid=fopen(fname,'r');
if fid==-1
  error('Cannot open file %s',fname);
end

% Pre-allocate space for records

numrecs_pre=17280; % one every 5 s for 24 h
recs.type=cell(1,numrecs_pre);
recs.dn=nan(1,numrecs_pre);
recs.freq=nan(12,numrecs_pre);
recs.arr=nan(12,numrecs_pre);
recs.pha=nan(12,numrecs_pre);
recs.swr=nan(12,numrecs_pre);
recs.pwr=nan(12,numrecs_pre);
recs.ref_freq=nan(2,numrecs_pre);
recs.cmnt=cell(1,numrecs_pre);
recs.pwr_a=nan(12,numrecs_pre);
recs.cap1=nan(12,numrecs_pre);
recs.cap2=nan(12,numrecs_pre);
recs.f_pha=nan(12,numrecs_pre);
recs.r_pha=nan(12,numrecs_pre);
recs.dds_amp=nan(12,numrecs_pre);
recs.dds_pha=nan(12,numrecs_pre);
recs.band=nan(12,numrecs_pre);
recs.mode=nan(12,numrecs_pre);
recs.beamdir=nan(12,numrecs_pre);


numrecs=0;
numlines=0;

while 1
  l=fgetl(fid);
  if l==-1
    % end of file
    break;
  end
  
  if strfind(l,'Experiment Setup :')==1
    numrecs=numrecs+1;
    recs=make_empty_record(recs,numrecs);
    recs.type{numrecs}='expsetup';
    
  elseif strfind(l,'Tuned values :')==1
    numrecs=numrecs+1;
    recs=make_empty_record(recs,numrecs);
    recs.type{numrecs}='tunedvals';
    
  elseif strfind(l,'Status at')==1
    numrecs=numrecs+1;  
    recs=make_empty_record(recs,numrecs);
    recs.type{numrecs}='status';
    recs.dn(numrecs)=datenum(l(length('Status at')+1:end));
    
  elseif strfind(l,'OPERATING TABLE')==1
    numrecs=numrecs+1;
    recs=make_empty_record(recs,numrecs);
    recs.type{numrecs}='optable';
    recs.dn(numrecs)=datenum(l(length('OPERATING TABLE')+1:end));

  elseif ~isempty(strfind(l,'Fast Freq shift'))
    numrecs=numrecs+1;
    recs=make_empty_record(recs,numrecs);
    recs.type{numrecs}='fastfreqshift';
    recs.dn(numrecs)=datenum([l(9:25) l(1:8)]);
    
  elseif regexp(l, ...
         '[0-3][0-9]-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-')==1
    numrecs=numrecs+1;
    recs=make_empty_record(recs,numrecs);
    recs.type{numrecs}='timestamp';
    recs.dn(numrecs)=datenum(l);
    
  elseif regexp(l,'Tx# ( [1-9]|1[0-2])')
    if numrecs ~= 0
      [tx,txrec]=parse_txline(l,recs.type{numrecs});
      fields=fieldnames(txrec);
      for fieldnum=1:length(fields)
        new_val=txrec.(fields{fieldnum});
        current_val=recs.(fields{fieldnum})(tx,numrecs);
        if ~isnan(current_val) && (new_val ~= current_val)
          error('Repeated Tx line with different data detected!');
        end
        recs.(fields{fieldnum})(tx,numrecs)=new_val;
      end
    else
      warning('Tx line without preceding record header');
    end
    
  elseif regexp(l,'Ref# [1-2]')
    if numrecs ~= 0
      [ref,ref_freq]=parse_refline(l);
      if ~isnan(recs.ref_freq(ref,numrecs))
        error('Duplicated Ref line detected! Unknown file format?');
      end
      recs.ref_freq(ref,numrecs)=ref_freq;
    else
      warning('Ref line without preceding record header');
    end
    
  elseif ~isempty(l)
    if (numrecs ~= 0) && strcmp(recs.type{numrecs},'timestamp')
      % This may be a comment line in an EROS log file
      if all(~isnan(recs.freq(:,numrecs)))
        % Comments only come *after* Tx lines and EROS logs always
        % include *all* Txs, even if not all are in use.
        if isempty(recs.cmnt{numrecs})
          recs.cmnt{numrecs}=l;
        else
          recs.cmnt{numrecs}=[recs.cmnt{numrecs} char(10) l];
        end
      end
    end
  end
  
  numlines=numlines+1;
  fprintf('Recs %4d Lines %4d\r',numrecs,numlines);
  fflush(1);
  
  
end

fclose(fid);

fprintf('\n');

% Trim off any redundant entries

if numrecs < numrecs_pre
  recs.type=recs.type(1:numrecs);
  recs.dn=recs.dn(1:numrecs);
  recs.freq=recs.freq(:,1:numrecs);
  recs.arr=recs.arr(:,1:numrecs);
  recs.pha=recs.pha(:,1:numrecs);
  recs.swr=recs.swr(:,1:numrecs);
  recs.pwr=recs.pwr(:,1:numrecs);
  recs.ref_freq=recs.ref_freq(:,1:numrecs);
  recs.cmnt=recs.cmnt(1:numrecs);
  recs.pwr_a=recs.pwr_a(:,1:numrecs);
  recs.cap1=recs.cap1(:,1:numrecs);
  recs.cap2=recs.cap2(:,1:numrecs);
  recs.f_pha=recs.f_pha(:,1:numrecs);
  recs.r_pha=recs.r_pha(:,1:numrecs);
  recs.dds_amp=recs.dds_amp(:,1:numrecs);
  recs.dds_pha=recs.dds_pha(:,1:numrecs);
  recs.band=recs.band(:,1:numrecs);
  recs.mode=recs.mode(:,1:numrecs);
  recs.beamdir=recs.beamdir(:,1:numrecs);
end

% Patch the missing array information in PET log entries using
% the values from the previous 'Experiment setup' table.

esidx=find(strcmp(recs.type,'expsetup'));
if ~isempty(esidx)
  idx=find(strcmp(recs.type,'status') | strcmp(recs.type,'optable') ...
           | strcmp(recs.type,'tunedvals'));
  for k=idx
    if ~all(isnan(recs.arr(:,k)))
      continue; % Skip early EROS entries which DO have the array
    end
    % Use expsetup record with greatest index less than index of this record
    j=find(esidx < k);
    esidx2=esidx(j(end));
    recs.arr(:,k)=recs.arr(:,esidx2);
  end

end

% Ensure a new record has all fields initialised properly.
% If new fields are added, update this function!

function R=make_empty_record(R,n)

R.type{n}='';
R.dn(n)=nan;
R.freq(:,n)=nan(12,1);
R.arr(:,n)=nan(12,1);
R.pha(:,n)=nan(12,1);
R.swr(:,n)=nan(12,1);
R.pwr(:,n)=nan(12,1);
R.ref_freq(:,n)=nan(2,1);
R.cmnt{n}='';
R.pwr_a(:,n)=nan(12,1);
R.cap1(:,n)=nan(12,1);
R.cap2(:,n)=nan(12,1);
R.f_pha(:,n)=nan(12,1);
R.r_pha(:,n)=nan(12,1);
R.dds_amp(:,n)=nan(12,1);
R.dds_pha(:,n)=nan(12,1);
R.band(:,n)=nan(12,1);
R.mode(:,n)=nan(12,1);
R.beamdir(:,n)=nan(12,1);

function [tx,txrec]=parse_txline(l,rectype)

switch rectype
  case 'timestamp'
    [tx,txrec]=parse_txline_eros(l);
  case 'optable'
    [tx,txrec]=parse_txline_pet(l);
  case 'status'
    [tx,txrec]=parse_txline_status(l);
  case 'expsetup'
    [tx,txrec]=parse_txline_expsetup(l);
  case 'tunedvals'
    [tx,txrec]=parse_txline_pet(l);
  case 'fastfreqshift'
    [tx,txrec]=parse_txline_ffs(l);
  otherwise
    error('Unknown record type');
end

% Sanity checks

if (tx < 1) || (tx > 12)
  error('Invalid Tx number %d',tx);
end

if (txrec.freq < 2) || (txrec.freq > 9)
  error('Invalid frequency %f',txrec.freq);
end

% array 0 can mean the original (low-freq) array 1
if (txrec.arr < 0) || (txrec.arr > 3)
  error('Invalid array %d',txrec.arr);
end

if (txrec.pha < -180) || (txrec.pha > 180)
  error('Invalid phase %f',txrec.pha);
end

%if (txrec.pwr < -5) || (txrec.pwr > 150)
%  error('Invalid power %f',txrec.pwr);
%end
% Apparently power can have crazy (-ve) values sometimes.

% We don't check SWR because it can apparently have
% some crazy values.

function [ref,freq]=parse_refline(l)

% read the line, skipping the first 8 characters
[x,count]=sscanf(l(9:end),'%f %f');
if count ~= 2
  error('Could not parse reference data line');
end
  
% sanity check
ref=x(1);
if (ref < 1) || (ref > 2)
  error('Invalid reference number %d',ref);
end
  
freq=x(2);

function [tx,txrec]=parse_txline_eros(l)

% read the line, skipping the first 8 characters
[x,count]=sscanf(l(9:end),'%f');
if count < 6
  error('Could not parse Tx data line');
end
  
% sanity check
tx=x(1);

if (tx < 1) || (tx > 12)
  error('Invalid Tx number %d',tx);
end


txrec.freq=x(2);
txrec.arr=x(3);
txrec.pha=x(4);
txrec.swr=x(5);
txrec.pwr=x(6);

if count >= 13
  txrec.pwr_a=x(7);
  txrec.cap1=x(8);
  txrec.cap2=x(9);
  txrec.f_pha=x(10);
  txrec.r_pha=x(11);
  txrec.dds_amp=x(12);
  txrec.dds_pha=x(13);
end

function [tx,txrec]=parse_txline_pet(l)

% read the line, skipping the first 3 characters
[x,count]=sscanf(l(4:end),'%f %f %f %f %f %f %f %f %f');
if count < 9
  error('Could not parse Tx data line: %s',l);
end
  
% sanity check
tx=x(1);
if (tx < 1) || (tx > 12)
  error('Invalid Tx number %d',tx);
end
  
txrec.freq=x(2);
txrec.arr=nan;
txrec.pha=x(6);
txrec.swr=x(8);
txrec.pwr=x(9);

function [tx,txrec]=parse_txline_status(l)

% This is probably a PET log entry, but it could be an early
% EROS entry during the transitional phase of the system.

% Try reading it as a PET entry...

try
  [tx,txrec]=parse_txline_pet(l);
catch
  % OK, pad it with 4 spaces at the start and treat it as (new)
  % EROS entry
  [tx,txrec]=parse_txline_eros(['    ' l]);
end


function [tx,txrec]=parse_txline_expsetup(l)

% read the line, skipping the first 3 characters
[x,count]=sscanf(l(4:end),'%f %f %f %f %f %f %f %f');
if count ~= 8
  error('Could not parse Tx data line (%s)',l);
end
  
% sanity check
tx=x(1);
if (tx < 1) || (tx > 12)
  error('Invalid Tx number %d',tx);
end
  
txrec.freq=x(2);
txrec.arr=x(3);
txrec.band=x(4);
txrec.pwr=x(5);
txrec.mode=x(6);
txrec.beamdir=x(7);
txrec.pha=x(8);
txrec.swr=nan;

function [tx,txrec]=parse_txline_ffs(l)

[x,count]=sscanf(l,' Tx# %f %f');
if count ~= 2
  error('Could not parse Tx data line (%s)',l);
end

% sanity check
tx=x(1);
if (tx < 1) || (tx > 12)
  error('Invalid Tx number %d',tx);
end

if x(2)~=0
  txrec.freq=x(2);
else
  txrec.freq=nan;
end
txrec.arr=nan;
txrec.band=nan;
txrec.pwr=nan;
txrec.mode=nan;
txrec.beamdir=nan;
txrec.pha=nan;
txrec.swr=nan;

