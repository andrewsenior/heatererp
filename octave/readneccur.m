function [ns,nt,x,y,z,l,I]=readneccur(fname)
% READNECCUR  Read currents from NEC output file
%
% [ns, nt, x, y, z, l, I] = readneccur(fname)
%
% Read currents from file fname. The segment number and wire tags are in ns, nt.
% The segment centres and lengths are in x, y, z and l. The complex current is
% in I.

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

fid=fopen(fname);
if fid==-1
  error('cannot open input file');
end

% scan for start of current table

rp_found=0;

while 1
  l=fgetl(fid);
  if l==-1
    break;
  end

  if ~isempty(strfind(l,'- CURRENTS AND LOCATION -'))
    fprintf('found currents\n');
    rp_found=1;
    break;
  end
end

if rp_found==0
  fclose(fid);
  error('currents not found');
end

% skip 6 lines

for k=1:6
  l=fgetl(fid);
end

c=1;
RP=[];
while 1
  l=fgetl(fid);
  if l==-1
    break;
  end
  [x,count]=sscanf(l,'%f',[1 8]);
  if count ~= 8
    break;
  end

  RP(c,1:8)=x;
  c=c+1;
end

if ~isempty(RP)
  ns=RP(:,1);
  nt=RP(:,2);
  x=RP(:,3);
  y=RP(:,4);
  z=RP(:,5);
  l=RP(:,6);
  I=RP(:,7)+i*RP(:,8);
else
  ns=[];
  nt=[];
  x=[];
  y=[];
  z=[];
  l=[];
  I=[];
end

fclose(fid);

