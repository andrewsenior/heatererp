function erpmodel(t_wanted)
% ERPMODEL  Run Heater ERP model for specified time
%
% erpmodel(t_wanted)
%
% Where t_wanted is a date/time string in a format recognised by
% the DATENUM function, e.g. '30-Nov-2010 10:00:10'

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

more off

conf=getconfig;

log_dir=conf.log_dir;
model_dir=conf.model_dir;
results_dir=conf.results_dir;
plots_dir=conf.plots_dir;

plot_title='';
output_prefix='';

if ~isstruct(t_wanted)

  % Date/time for log entry given

  dn_wanted=datenum(t_wanted);

  % Find log file based on wanted date, or prompt user if this
  % doesn't work.

  log_name=[datestr(dn_wanted,'yyyy-mm-dd') '_tx.log'];
  log_name=[log_dir filesep log_name];

  if ~exist(log_name,'file')
    while true
      fprintf('Cannot find file %s\n', log_name);
      fprintf('Please enter the log file name with full path\n');
      fflush(stdout);
      s=input('File name or Enter to quit: ','s');
      if isempty(s)
        return
      end
      log_name=s;
      if exist(s,'file')
        break;
      end
    end
  end
  

  L=loadlogfromcache(log_name);

  % We can replace missing (NaN) transmitter powers with zero since
  % these transmitters are unused anyway.

  L.pwr(isnan(L.pwr))=0;

  k=find(L.pwr < 0);
  if ~isempty(k)
    L.pwr(k)=0;
    fprintf('WARNING! At least one log entry contained a negative Tx power.\n');
    fprintf('Negative powers have been set to zero.\n');
    fflush(stdout);
  end

  % Find the nearest entry to the wanted time when at least one
  % transmitter is on.

  % FIXME: this will only find the nearest within the current log file!

  k=find(sum(L.pwr) > 0);
  [dummy,j]=min(abs(L.dn(k)-dn_wanted));

  idx=k(j);

  fprintf('Nearest entry with a transmission is %s\n', ...
          datestr(L.dn(idx),'yyyy-mm-dd HH:MM:SS'));

  L1.dn=L.dn(idx);
  L1.freq=L.freq(:,idx);
  L1.arr=L.arr(:,idx);
  L1.pha=L.pha(:,idx);
  L1.swr=L.swr(:,idx);
  L1.pwr=L.pwr(:,idx);
  L1.ref_freq=L.ref_freq(:,idx);
  L=L1;

else
  % An actual log entry given

  L=t_wanted;
  if ~all(isfield(L,{'dn','freq','arr','pha','swr','pwr','ref_freq'}))
    error('Log entry does not contain all required fields');
  end

  plot_title=input('Enter a plot title, or blank for default: ','s');

  fprintf('Enter a name for the output file (excluding path and extension)\n');
  fflush(stdout);
  output_prefix=input('or blank for default: ','s');

  % The default title and file name require a valid timestamp.
  % Check for valid timestamp, prompt if missing.
  if (isempty(plot_title) || isempty(output_prefix)) ...
     && (isempty(L.dn) || ~isfinite(L.dn))
    fprintf('Log entry lacks a valid timestamp.\n');
    fflush(stdout);
    s=input('Please enter a date and time: ','s');
    L.dn=datenum(s);
  end

  L.pwr(isnan(L.pwr))=0;

  k=find(L.pwr < 0);
  if ~isempty(k)
    L.pwr(k)=0;
    fprintf('WARNING! The log entry contained one or more negative Tx powers.\n');
    fprintf('Negative powers have been set to zero.\n');
    fflush(stdout);
  end

end

% Try to guess missing array information based on frequency
if any(isnan(L.arr))
  fprintf('WARNING! Array information is missing\n');
  fprintf('         Assuming array based on frequency\n');
  fflush(1);
  
  k=find(isnan(L.arr));
  arr_freq=2*ones(length(k),1);
  arr_freq(L.freq(k) >= 5.423)=1;
  L.arr(k)=arr_freq;
end

% Find the active groups of transmitters

txg=txgroups(L);

% Run the model for each group

for gnum=1:length(txg)

  % make a temporary directory
  tmp_dir=tempname('','erpmodel');
  [status,output]=system(['mkdir ' tmp_dir]);
  if status ~= 0
    error('Unable to create temporary directory: %s',output);
  end
  fprintf('using temporary directory %s\n',tmp_dir);
  cur_dir=pwd;
  cd(tmp_dir);

  if isempty(output_prefix)
    output_basename=datestr(L.dn,'yyyy-mm-dd_HHMMSS');
  else
    output_basename=output_prefix;
  end
  output_basename=[output_basename ...
                   sprintf('_A%d_%.8f',txg(gnum).arr,txg(gnum).freq)];
  
  
  try
  
    % create the input file
    modname=sprintf('a%d-simple-withcrossover',txg(gnum).arr);
    modfname=['wires-' modname '.txt'];
    modfname=[model_dir filesep modfname];
    fprintf('model file: %s\n',modfname);
    wires2nec(modfname,'nec_in.txt',conf.ant_segments(txg(gnum).arr+1), ...
              conf.ant_wire_radius(txg(gnum).arr+1),false);
    mkexcitation(modname,'nec_in.txt',txg(gnum));
  
    % run NEC2
  
    fprintf(' *** running NEC2 ***\n');
    fflush(1);
  
    switch conf.model_engine
      case 'nec2'
        nec_cmd=[conf.nec2_bin ' nec_in.txt nec_out.txt'];
      case 'nec2c'
        nec_cmd=[conf.nec2c_bin ' -i nec_in.txt -o nec_out.txt'];
      otherwise
        error('Unknown model engine: %s',conf.model_engine);
    end

    [status,output]=system(nec_cmd);
    if status ~= 0
      error('Error %d running NEC2: %s',status,output);
    end
  
    fprintf(' *** NEC2 completed successfully ***\n');
    fflush(1);

    % Archive the results.
    % If the ZIP file already exists, the files in it will be replaced
    % with the new ones.

    zipname=[results_dir filesep output_basename '.zip'];         
      
    [status,output]=system(['zip ' zipname ' nec_in.txt nec_out.txt']);
    if status ~= 0
      error('Error %d running zip: %s',status,output);
    end

    % process the output file
  
    [T,P,G,Et,Ep]=readnecrp('nec_out.txt');

  catch
    % If something went wrong, ensure the temporary directory
    % is removed.

    cleanup_tmpdir(tmp_dir,cur_dir);
    rethrow(lasterror);
  
  end

  % delete the temporary directory

  cleanup_tmpdir(tmp_dir,cur_dir);

  % analyse the NEC2 results
  
  [GL,GR,G_avg]=analyseradpat(T,P,G,Et,Ep);
  
  % average gain correction (perfect ground)
  
  G_corr=10*log10(G_avg/2);
  GL=GL-G_corr;
  GR=GR-G_corr;
  
  % produce annotate plot
  
  plotrp(T,P,GL,GR,txg(gnum),10*log10(G_avg),plot_title);
  
  pdfname=[plots_dir filesep output_basename '.pdf'];      
  print('-dpdf',pdfname);
  fprintf('created plot %s\n',pdfname);
  fflush(1);
end

% Restore previous current directory and delete the temporary directory

function cleanup_tmpdir(tmp_dir,cur_dir)

cd(cur_dir);
  
[status,output]=system(['rm -r ' tmp_dir]);
if status ~= 0
  error('Unable to remove temporary directory %s: %s\n',tmp_dir,output);
end
  



