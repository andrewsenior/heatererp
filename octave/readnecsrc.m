function [nt,ns,v,j]=readnecsrc(fname)
% READNECSRC  Read source data from NEC output file
%
% [nt, ns, v, j] = readnecsrc(fname)
%
% Read source data from file fname. The tag and segment numbers of each source
% are returned in nt and ns. The complex voltages and currents are returned in
% v and j.

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

fid=fopen(fname);
if fid==-1
  error('cannot open input file');
end

% scan for start of source data

src_found=0;

while 1
  l=fgetl(fid);
  if l==-1
    break;
  end

  if ~isempty(strfind(l,'- ANTENNA INPUT PARAMETERS -'))
    fprintf('found source data\n');
    src_found=1;
    break;
  end
end

if src_found==0
  fclose(fid);
  error('source data not found');
end

% skip 3 lines

for k=1:3
  l=fgetl(fid);
end

c=1;
SD=[];
failval=0;
while 1
  l=fgetl(fid);
  if (l==-1) || isempty(l)
    break;
  end

  if length(l) < 2*6+4*12
    break;
  end
  
  % tag and segment numbers in cols. 2-6 and 8-12 

  [x,count]=sscanf(l(2:6),'%f',1);
  if count ~= 1
    failval=1;
    break;
  end
  SD(c,1)=x;

  if l(8)~='*'
    [x,count]=sscanf(l(8:12),'%f',1);
  else
    [x,count]=sscanf(l(9:12),'%f',1);
  end
  if count ~= 1
    failval=2;
    break;
  end
  SD(c,2)=x;

  % real,imag voltage and current in cols. 13-24, 25-36, 37-48, 49-60

  [x,count]=sscanf(l(13:24),'%f',1);
  if count ~= 1
    failval=3;
    break;
  end
  SD(c,3)=x;

  [x,count]=sscanf(l(25:36),'%f',1);
  if count ~= 1
    failval=4;
    break;
  end
  SD(c,4)=x;

  [x,count]=sscanf(l(37:48),'%f',1);
  if count ~= 1
    failval=5;
    break;
  end
  SD(c,5)=x;

  [x,count]=sscanf(l(49:60),'%f',1);
  if count ~= 1
    failval=6;
    break;
  end
  SD(c,6)=x;

  c=c+1;
end

if failval ~= 0
  error('Error when reading file (code=%d)',failval);
end

if ~isempty(SD)
  nt=SD(:,1);
  ns=SD(:,2);
  v=SD(:,3) + i*SD(:,4);
  j=SD(:,5) + i*SD(:,6);
else
  nt=[];
  ns=[];
  v=[];
  j=[];
end

fclose(fid);

