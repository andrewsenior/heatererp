function G=txgroups(S)
% TXGROUPS  Find groups of active transmitters

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% First find frequencies in use, then group by array

ufreq=unique(S.freq);

gidx=0;

for k=1:length(ufreq)

  j=find(S.freq==ufreq(k));
  uarr=unique(S.arr(j));
  
  for n=1:length(uarr)
  
    j=find((S.freq==ufreq(k)) & (S.arr==uarr(n)));
    if any(S.pwr(j) > 0)

      % This is an active group
      gidx=gidx+1;
      G(gidx).freq=ufreq(k);
      G(gidx).arr=uarr(n);
      G(gidx).tx=j;
      G(gidx).pha=S.pha(j);
      G(gidx).swr=S.swr(j);
      G(gidx).pwr=S.pwr(j);
      G(gidx).dn=S.dn;
     
    end
  
  end

end

fprintf('--- Before reference check ---\n');
fprintf('There are %d groups\n',gidx);

for k=1:gidx
  printgroup(G,k);
end

fprintf('\nThe references are:\n');
fprintf('1: %.6f MHz  2: %.6f MHz\n\n',S.ref_freq(1),S.ref_freq(2));

if any(isnan(S.ref_freq))
  fprintf('WARNING! Reference data incomplete.\n');
  fprintf('         Phases will be *assumed* correct!\n');
end

% Check that the transmitters in a group all have an appropriate
% phase reference

group_OK=true(size(G));

for k=1:length(G)
  if (any(G(k).tx <= 6) && (G(k).freq ~= S.ref_freq(1)) ...
      && ~isnan(S.ref_freq(1)))
    group_OK(k)=false;
  elseif (any(G(k).tx >= 7) && (G(k).freq ~= S.ref_freq(2)) ...
      && ~isnan(S.ref_freq(2)))
    group_OK(k)=false;
  end
end

G=G(group_OK);

fprintf('--- After reference check ---\n');
fprintf('There are %d groups\n',length(G));

for k=1:length(G)
  printgroup(G,k);
end    


function printgroup(G,gnum)

fprintf('Group %d\n',gnum)
fprintf('  Frequency %.6f MHz Array %d\n',G(gnum).freq,G(gnum).arr);
fprintf('  Tx  ');
fprintf('%4d ',G(gnum).tx);
fprintf('\n');
fprintf('  Pha ');
fprintf('%4.0f ',G(gnum).pha);
fprintf('\n');
fprintf('  Pow ');
fprintf('%4.0f ',G(gnum).pwr);
fprintf('\n');

