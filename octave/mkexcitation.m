function mkexcitation(modname,outname,txg)
% MKEXCITATION  Make NEC2 excitation data for Heater antenna model
%
% mkexcitation(modname, outname, txg)
%
% Generate NEC2 excitation data for the heater antenna model specified
% by 'modname' and the transmitter group structure (see TXGROUPS) 'txg'.
% The output is written to the file 'outname'.

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

if isempty(strfind(modname,'simple'))
  error('Only ''simple'' models are supported');
end

% Open file: append to an existing file

fid=fopen(outname,'a');
if fid==-1
  error('Cannot open output file %s\n',outname);
end

% Excitation

%  *** IMPORTANT! ***
% The following code makes certain assumptions about the numbering of the wires
% and their location in the array.
%
% It is assumed that:
%
% 1. The wires are listed by row, starting with the northernmost row.
% 2. Within a row, the northeast-southwest oriented dipoles are all listed first
%    followed by the northwest-southeast dipoles.
% 3. The NE-SW dipoles are connected to the ODD numbered Tx and the NW-SE
%    dipoles to the EVEN numbered Tx.
% 4. In Array 1, where each row is divided into two identical "subrows", all
%    the NE-SW dipoles from BOTH subrows are listed first, followed by all the
%    NW-SE dipoles.
% 5. The wire(s) which compose(s) a dipole are listed so that the "reference
%    direction" for the current (see the NEC manual) is from the north end to
%    the southern end of each dipole, regardless of orientation. This means that
%    if the odd and even Tx phases are the same for a given row, the northern
%    ends of all dipoles are in phase.
 

% Copy the transmitter parameters from the group structure.
% Missing (off) transmitters are replaced with ones producing
% a very low power (currently 1 W) to prevent NEC2 doing strange
% things with the sources.

freq=txg.freq;
pwr=zeros(1,12);
pha=zeros(1,12);

pwr(txg.tx)=txg.pwr;
pha(txg.tx)=txg.pha;

pwr(pwr==0)=1e-3;

V_source=sqrt(pwr/max(pwr)); % normalised source voltage

if isempty(strfind(modname,'withcrossover'))
  % The "simple" model
  %
  % This model has one wire per dipole.
  % An applied E-field voltage source is placed in the centre segment of
  % each dipole.

  nsegments=11; % FIXME: hard-coded number of segments!

  if isempty(strfind(modname,'a1'))
    % Array 2 or 3
    for tx=1:12
      V=V_source(tx)*exp(i*pha(tx)*pi/180);
      for k=1:12
        tag=(tx-1)*6+k;
        fprintf(fid, 'EX 0 %d %d 0 %.3f %.3f\n',tag,(nsegments+1)/2, ...
                real(V),imag(V));
      end
    end
  else
    % Array 1 
    for tx=1:12
      V=V_source(tx)*exp(i*pha(tx)*pi/180);
      for k=1:24
        tag=(tx-1)*24+k;
        fprintf(fid, 'EX 0 %d %d 0 %.3f %.3f\n',tag,(nsegments+1)/2, ...
                real(V),imag(V));
      end
    end
  end

else
  % The "simple with crossover" model.
  %
  % This model has 2 long and 2 short wires per dipole
  % A current slope discontinuity voltage source is placed between the two
  % single-segment "crossover" wires at the centre of each dipole.
  % Strictly-speaking this is wrong, since the two segments each side of the
  % source are supposed to be parallel and this is not quite true.  

  if isempty(strfind(modname,'a1'))  
    % Array 2 or 3
    for tx=1:12
      V=V_source(tx)*exp(i*pha(tx)*pi/180);
      for k=1:6
        tag=(tx-1)*24+(k-1)*4+3; % tag number of second short wire
        fprintf(fid, 'EX 5 %d %d 0 %.3f %.3f\n',tag,1,real(V),imag(V));
      end
    end
  else
    % Array 1
    for tx=1:12
      V=V_source(tx)*exp(i*pha(tx)*pi/180);
      for k=1:24
        % tag number of second short wire
        tag=(tx-1)*96+(k-1)*4+3;
        fprintf(fid, 'EX 5 %d %d 0 %.3f %.3f\n',tag,1,real(V),imag(V));
      end
    end
  end

end

% Use extended thin-wire kernel
%fprintf(fid, 'EK 0\n');

% Interaction approximation distance (wavelengths)
%fprintf(fid, 'KH 0 0 0 0 20.\n');

% Wire conductivity
%cond=2.5e7; % aluminium, as used in EZNEC model
%fprintf(fid, 'LD 5 0 0 0 %.3f\n', cond);

% Frequency

fprintf(fid, 'FR 0 1 0 0 %.3f 0.\n',freq);

% Ground

fprintf(fid, 'GN 1\n');

% Radiation pattern

fprintf(fid, 'RP 0 91 360 1001 0. 0. 1. 1.\n');

fprintf(fid, 'EN\n');

fclose(fid);

