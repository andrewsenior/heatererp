function setupconfig(confscript,C)
% SETUPCONFIG  Interactively set up user configuration file

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

fprintf('\nPlease answer the following questions to set up the software.\n\n');

fprintf('The directory where the antenna model files are stored\n');
fprintf('is currently: %s\n',C.model_dir);
fprintf('Enter a new path, or blank to leave it unchanged.\n');
fflush(stdout);
s=input('> ','s');
if ~isempty(s)
  C.model_dir=s;
end

fprintf('\nDo you want to use nec2c as the model engine?\n');
fprintf('Enter Y if you do, anything else or blank if not.\n');
fflush(stdout);
s=input('> ','s');
if strcmp(s,'Y')
  C.model_engine='nec2c';
  fprintf('Using nec2c as the model engine.\n');
else
  C.model_engine='nec2';
  fprintf('\nThe path to the NEC2 binary (executable)\n');
  fprintf('is currently: %s\n',C.nec2_bin);
  fprintf('Enter a new path, or blank to leave it unchanged.\n');
  fflush(stdout);
  s=input('> ','s');
  if ~isempty(s)
    C.nec2_bin=s;
  end
end

fprintf('\nThe directory where the transmitter log files are stored\n');
fprintf('is currently: %s\n',C.log_dir);
fprintf('Enter a new path, or blank to leave it unchanged.\n');
fflush(stdout);
s=input('> ','s');
if ~isempty(s)
  C.log_dir=s;
end

fprintf('\nThe next two paths are for storing ERP model results.\n');
fprintf('You must ensure that the paths chosen exist before\n');
fprintf('you run the model!\n\n');

fprintf('The directory where plots are stored\n');
fprintf('is currently: %s\n',C.plots_dir);
fprintf('Enter a new path, or blank to leave it unchanged.\n');
fflush(stdout);
s=input('> ','s');
if ~isempty(s)
  C.plots_dir=s;
end

fprintf('\nThe directory where NEC2 results are stored\n');
fprintf('is currently: %s\n',C.results_dir);
fprintf('Enter a new path, or blank to leave it unchanged.\n');
fflush(stdout);
s=input('> ','s');
if ~isempty(s)
  C.results_dir=s;
end

fprintf('\n');

confdir=fileparts(confscript);
if ~exist(confdir,'dir')
  [status,output]=system(sprintf('mkdir -p %s',confdir));
  if status ~= 0
    fprintf('Could not create configuration directory: %s\n',output);
    fprintf('Configuration has NOT been saved.\n');
    fflush(stdout);
    return
  end 
end

[fid,msg]=fopen(confscript,'w');
if fid==-1
  fprintf('Could not create configuration file %s (%s)\n',confscript,msg);
  fprintf('Configuration has NOT been saved.\n');
  fflush(stdout);
  return
end

fprintf(fid, '%% Configuration script for Heater ERP model\n');

% Currently, only textual fields are saved. This includes all the
% file/directory locations.

fields=fieldnames(C);
for k=1:length(fields)
  if ischar(C.(fields{k}))
    fprintf(fid,"%s='%s';\n",fields{k},C.(fields{k}));
  end
end

fclose(fid);

fprintf('Configuration saved to %s\n',confscript);
fflush(stdout);


