function C=getconfig(do_setup)
% GETCONFIG  Return structure with configuration information

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

persistent C

if nargin < 1
  do_setup=false;
end

if isempty(C)

  % Set some defaults, then try to run the user's configuration
  % script.

  % directory containing model files
  C.model_dir=[getmyrootdir filesep 'models'];

  % path to NEC2 binary
  %C.nec2_bin='/home/andrew/devel/nec/nec-2/nec/nec2';
  C.nec2_bin='/the/real/path/to/nec2';

  % path to NEC2C binary
  C.nec2c_bin='nec2c';

  % which model engine to use
  % 'nec2' for Fortran-based version, 'nec2c' for C-based version
  C.model_engine='nec2';

  data_root=fullfile(getenv('HOME'),'heatererp');

  % directory containing transmitter logs
  C.log_dir=[data_root filesep 'logs'];

  % directory in which to store result files
  C.results_dir=[data_root filesep 'results'];

  % directory in which to store plots
  C.plots_dir=[data_root filesep 'plots'];

  % Antenna parameters
  % Since "Array 0" can refer to the original (low-frequency) Array 1,
  % the vectors here should be indexed as vec(array_number+1) to obtain
  % the appropriate value.

  C.ant_segments=[10 6 10 10];
  C.ant_wire_radius=4.5e-3*[1 1 1 1];

  confscript=fullfile(getenv('HOME'),'.config','heatererp','conf.m');

  if ~exist(confscript,'file') && do_setup
    setupconfig(confscript,C);
  end

  if exist(confscript,'file')
    run(confscript);
    disp('Loaded user configuration');
  else
    warning('User configuration file %s does not exist, using defaults', ...
            confscript);
  end

  % Store final parameters into configuration structure

  conf_params={'model_dir','nec2_bin','log_dir','results_dir', ...
               'plots_dir','ant_segments','ant_wire_radius', ...
               'model_engine','nec2c_bin'};

  for k=1:length(conf_params)
    if exist(conf_params{k},'var')
      C.(conf_params{k})=eval(conf_params{k});
    end
  end
end

function d=getmyrootdir

% Find root directory of code installation

d=mfilename('fullpath');
idx=strfind(d,filesep);
if length(idx) < 2
  % can't safely determine root
  d='';
else
  % strip off last two elements in path
  d=d(1:idx(end-1)-1);
end

