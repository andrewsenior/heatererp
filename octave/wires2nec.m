function wires2nec(wfname,nfname,nsegs,wrad,do_plot)
% WIRES2NEC  Convert list of wires into NEC input format
%
% wires2nec(wfname, nfname, nsegs, wrad)
%
% Read wires from file wfname, write NEC format to nfname
%
% nsegs is optional and if specified, overrides the default number
% of segments per wire, or the number of segments specified in the
% input file except where this is 1.
%
% wrad is optional and if specified, overrides the default wire radius
% in metres for all wires.

% Copyright 2016 Andrew Senior
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

nsegments=9; % default no. of segments, if not specified in input file
if nargin > 2
  nsegments=nsegs;
end

W=load(wfname);

fid=fopen(nfname,'w');
if fid==-1
  error('cannot open output file');
end

nwires=size(W,1);

if do_plot
  figure
end

fprintf(fid, 'CE %s\n',wfname);

for k=1:nwires
  
  if do_plot
    line(W(k,[1 4]),W(k,[2 5]),W(k,[3 6]),'color','b');
    text(W(k,1),W(k,2),W(k,3),num2str(k));
  end

  if size(W,2)==8
    % number of segments specified in input file
    nsegments=W(k,8);
    if (nsegments > 1) && (nargin > 2)
      nsegments=nsegs;
    end
  end

  if nargin>3
    radius=wrad;
  else
    radius=W(k,7);
  end

  fprintf(fid, 'GW %d %d %.3f %.3f %.3f %.3f %.3f %.3f %.3g\n',k,nsegments, ...
          W(k,1),W(k,2),W(k,3),W(k,4),W(k,5),W(k,6),radius);
 
end

fprintf(fid, 'GE\n');

fclose(fid);

if do_plot
  xlabel('X (m)');
  ylabel('Y (m)');
  title(wfname);
end


