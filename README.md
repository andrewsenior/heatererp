# EISCAT HF Facility ERP Model

This package models the effective radiated power (ERP) and radiation pattern
of the EISCAT HF Facility ionospheric "heater". Key features are:

* Automatically reads transmitter parameters from log files
* Generates radiation pattern plots in PDF format

## System Requirements

The package is based on GNU Octave running under Linux and has been developed
and tested with Octave versions 3.2.4 and 3.8.1 under Ubuntu 12.04 and 14.04.

The model uses the public-domain NEC-2 code for the electromagnetic modelling
of the antenna array. This must be installed separately from the Debian source
package and then patched and built. Instructions on how to do this on
Debian-based systems (it should be straightforward!) are included in the
documentation.

## Installation

* Clone the repository or export it as a ZIP file and put it in your chosen
location
* Follow the instructions in the HTML documentation (open doc/index.html in
a browser) for how to set up the package

## Licensing

Note that three separate licences apply to the package:

* The code itself is under the Apache License 2.0
* The antenna models for which EISCAT owns the copyright are under the Creative
Commons CC-BY-NC-SA 4.0 License. Note that **this forbids commercial use of the
model files**.
* The patch for the NEC-2 code is placed in the public domain via CC0
disclaimer.

