<html>
<head>
<title>Installation: EISCAT Heater ERP model</title>
<link rel="stylesheet" type="text/css" href="heatererp.css" />
</head>
<h1>Installation</h1>
<p>If the software was obtained from the <em>git</em> repository then it will
be located in the directory where it was put by <code>git clone</code> and
should contain the following directory structure
<pre>
<code>/some/path/doc -- this documentation</code>
<code>          /bin -- binary/executable/script files</code>
<code>          /octave -- Octave source files</code>
<code>          /nec-patch -- patch for Debian/Ubuntu nec package</code>
</pre>
where <code>/some/path</code> is the directory it was cloned into.</p>

<p>In the <code>bin</code> directory is a shell script, <code>erpmodel.sh</code>
, which is used to start the software. You should create a symbolic link to
this file somewhere on your shell's search path,
e.g. <code>/usr/local/bin/erpmodel</code>. The software can then be started
simply by typing <code>erpmodel</code> at the shell prompt.</p>

<h2>NEC installation</h2>
<p>The software uses NEC (Numerical Electromagnetics Code) as its model engine.
Two different versions of NEC are supported:
<ul>
<li>The Debian/Ubuntu version based on the original Fortran source</li>
<li>A C translation of the original known as <code>nec2c</code></li>
</ul>
You may choose which model engine is used during the setup procedure after
installation, as described below. It is possible to change between model engines
by editing the configuration file.</p>

<h3>nec2c (the easy way?)</h3>
<p>On Debian/Ubuntu systems, <code>nec2c</code> may be installed using the
system package manager in the normal way, e.g.
<pre>
<code>sudo apt-get install nec2c</code>
</pre>
Unlike the <code>nec2</code> package, <code>nec2c</code> does not require any
patching to work. This makes installation much easier. <strong>However, the
version of <code>nec2c</code> supplied with Ubuntu 14.04 runs very slowly
compared to the Fortran-based version </strong> (this may be due to an error in
the Debian package that means it is built without optimisation).
If this is a problem for you, you can build nec2c from source yourself and
point the ERP model software at the binary by editing the
<code>nec2c_bin</code> line in the configuration file.
Instructions on building nec2c are not included here.</p>

<h3>The Debian/Ubuntu NEC</h3>
<p>The software is designed to work with the version of NEC from the Debian/
Ubuntu <code>nec</code> package. Unfortunately the standard version of this
package does not permit enough sources to be applied to the antenna structure to
model the HF heating arrays. A patch is supplied in the <code>nec-patch</code>
subdirectory of the ERP model installation.</p>

<h4>Obtaining the NEC source code</h4>

<h5>Debian/Ubuntu-based systems</h5>

<p>If you are running Debian or Ubuntu or a distribution derived from them,
first install the <code>nec</code> <em>source</em> package into a location of
your choice by doing
<pre>
<code>cd /path/of/your/choice</code>
<code>apt-get source nec</code>
</pre>
This will install the NEC source in the directory
<code>/path/of/your/choice</code>. You may have to enable the &quot;source&quot;
repositories before you can do this and you may need to install some other
packages so that <code>apt-get</code> can correctly set up the source (you
should receive a warning message if this is the case).</p>

<h5>Distributions lacking the <code>nec</code> package</h5>

<p>Recent Ubuntu distributions (e.g. 16.04) and non-Debian/Ubuntu based
distributions do not have the <code>nec</code> package or if they do it may not
be based on the same source code. For these distributions, the process is a bit
more complicated.</p>

<p>First, create a directory where you want to install NEC and change into it
<pre>
<code>mkdir /path/of/your/choice</code>
<code>cd /path/of/your/choice</code>
</pre></p>

<p>Now, download the Debian <code>nec</code> source files into this directory.
They can be found at
<a href="https://packages.debian.org/source/jessie/nec">
https://packages.debian.org/source/jessie/nec</a>. The two files required are
<code>nec_2.orig.tar.gz</code> and <code>nec_2-16.diff.gz</code>. Unpack the
original source code and apply the Debian patch
<pre>
<code>tar zxvf nec_2.orig.tar.gz</code>
<code>cp -r nec-2.orig nec-2</code>
<code>zcat nec_2-16.diff.gz | patch -p0</code>
</pre>
You may see a warning from <code>patch</code> about patching a read-only file;
this is normal.<p>

<h4>Applying the patch</h4>

<p>You can now apply the patch in the following way
<pre>
<code>cd /path/of/your/choice/nec-2</code>
<code>patch -p1 &lt; /some/path/nec-patch/NEC-PATCH</code>
</pre>
where <code>/some/path</code> is the path where the ERP model software was
installed above. You may see a warning about trying to patch a read-only file;
this is normal. Finally, the NEC software should be built using
<pre>
<code>make FC=fort77 F77=fort77</code>
</pre>
The code is built using the <em>f2c</em> Fortran-to-C translator and you may
have to install the <code>f2c</code> package (or equivalent on non-Debian
distributions) in order to build the NEC code. <code>f2c</code> provides the
<code>fort77</code> script that makes <em>f2c</em> run like a Fortran compiler.
The arguments to <code>make</code> ensure that the correct compiler is used (if
the compiler is not specified explicitly like this, then on systems that have
other Fortran compilers installed, e.g. GNU Fortran, another compiler may be
called instead, depending on how it has been installed. This can result in a
non-functioning NEC program).</p>

<p>Note that there is no need to
do <code>make install</code> after running <code>make</code>. Indeed, doing so
would be inadvisable if you already have the <code>nec</code> package installed
for some other purpose.</p>



<h2>Setup</h2>
<p>After installation, the first time a user runs the software, they will be
prompted to configure it by specifying the location of various files such as
the transmitter logs, the antenna array model files, where plots and results
should be stored and the path to the NEC executable. These settings are stored
individually for each user.</p>

<p>If you installed the NEC software according to the instructions above, the
path to the NEC excutable will be
<code>/path/of/your/choice/nec-2/nec/nec2</code></p>
<p>If you choose to use <code>nec2c</code>, it is assumed that the
<code>nec2c</code> executable is already on your search path (i.e. you can run
it by typing <code>nec2c</code> at the shell prompt). If this is not the case,
you must manually edit the configuration file (see below) and set the
<code>nec2c_bin</code> line appropriately.

<h3>Manual Configuration</h3>
<p>If you made a mistake during the setup process or you later need to change
the location where the software stores results, you can edit the configuration
file. The file is <code>$HOME/.config/heatererp/conf.m</code> where
<code>$HOME</code> is your home directory. The file is actually an Octave
script that sets the values of certain variables to indicate the path to the
NEC executable, where to find and store files, etc. For example, to change the
path to the NEC executable, edit the definition of the variable <code>
nec2_bin</code>.</p>

<h2>Testing</h2>
<p>To check that your installation is working, a sample log file containing a
synthetic test case is provided. This is located in the <code>testing</code>
subdirectory of the directory where you installed the model. The file is
called <code>1970-01-01_tx.log</code>.</p>

<p>Copy this file to (or make a symlink to it in) your log file directory which
you configured during the setup process. You will need to create this directory
if it does not already exist. Then start the model by giving the
<code>erpmodel</code> command at the shell prompt (assuming you installed it
as described above). Then at the Octave prompt, give this command
<pre>
<code>erpmodel('01-Jan-1970 00:00:00')</code>
</pre>
This will run the model for a hypothetical case where all the transmitters
are used with identical power and perfect phasing to generate a vertical beam
on 5.423 MHz using Array 1.</p>

<p>A number of messages will appear in the Octave terminal window, the most
important being <code>*** running NEC2 ***</code>. This message should be
followed some time later by the message
<code>*** NEC2 completed successfully ***</code>. This may take several minutes
depending on the speed of your computer and may be especially slow if you are
using <code>nec2c</code> which has been built without optimisation (e.g. it
takes about 9 minutes on an Intel Core i5 650 at 3.2 GHz!). If you see an error
message instead then this probably means that something is wrong with your NEC
installation. Make sure that you set the correct path to the NEC executable
during the setup process. If necessary, edit the configuration file to change
the path.</p>

<p>When the modelling is complete, a plot will appear on the screen. A PDF
file of the plot will be created in your "plots" directory and a ZIP file
containing the full NEC-2 model output will be created in your "results"
directory.</p>

<hr>
<p><a href="index.html">Contents</a></p>
<body>
</body>
</html>

